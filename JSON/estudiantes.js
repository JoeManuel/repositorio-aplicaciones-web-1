document.querySelector('#boton').addEventListener('click', mostrardatos);

function mostrardatos(){
    const xhttp = new XMLHttpRequest();
    xhttp.open('GET','estudiantes.json',true);
    xhttp.send();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
        let datos = JSON.parse(this.responseText);

        let fila = document.querySelector('#fila');
        fila.innerHTML = '';
        for(let item of datos){
            fila.innerHTML +=`
            <tr>
                <td>${item.nombre}</td>
                <td>${item.cedula}</td>
                <td>${item.direccion}</td>
                <td>${item.correo}</td>
                <td>${item.telefono}</td>
                <td>${item.curso}</td>
                <td>${item.paralelo}</td>
            </tr>
            `
        }
        }
    }
}